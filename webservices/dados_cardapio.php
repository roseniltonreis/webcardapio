<?php

	header('Content-Type: application/json');
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day

	include("config/connection.php");

	$query = mysqli_query($link, "SELECT * FROM item");
	// $query = mysqli_query($link, "SELECT * FROM item WHERE user_id = '1'");
	$result = array();
	while ($res = mysqli_fetch_assoc($query)) {
		$rows[] = $res;
	}
	echo json_encode($rows);

?>