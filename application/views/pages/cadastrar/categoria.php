<div class="page-header">
 <h1>Cadastro de Categorias</h1>
</div>
<?php $user_id = $this->session->userdata("user_id"); ?>
<form role="form" method="POST" id="form_categoria">
 <input type="hidden" name="user_id" value="<?php echo $user_id;?>">
 <div class="form-group">
  <label for="nome_categoria">Nome da categoria</label>
  <input type="nome_categoria" required autocomplete="off" name="nome_categoria" class="form-control" id="nome_categoria">
</div>

<button type="submit" class="btn btn-default">Salvar</button>
<button  class="btn btn-danger" onclick="window.location='<?php echo base_url("categoria/listar");?>';return false;">Cancelar</button>
</form>


<script type="text/javascript">

 $( document ).ready(function() {

  $("#form_categoria").validate({

   submitHandler:function(form, e) {

    e.preventDefault();
    e.stopPropagation();

    $.ajax({
     url: '<?php echo base_url("categoria/salvar");?>',
     type: 'POST',
     data: $(form).serialize(),
   })
    .always(function(data) {
     if(data == "ok"){
      swal({
       title: "Categoria salva com sucesso!",
       type: "success"
     },function(){
       window.location.href = "<?php echo base_url('categoria/listar');?>";
     });
    } else{
      swal({
       title: data,
       type: "error"
     });
    }
  });
    

  },
  highlight: function(element) {
    $(element).closest('.form-group').addClass('has-error');
  },
  unhighlight: function(element) {
    $(element).closest('.form-group').removeClass('has-error');
  },
          // errorElement: 'span',
          errorClass: 'help-block',
          errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
             error.insertAfter(element.parent());
           } else {
             error.insertAfter(element);
           }
         }
       });

  }); //fim document.ready
</script>