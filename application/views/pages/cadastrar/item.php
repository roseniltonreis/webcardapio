<div class="page-header">
	<h1>Cadastro de item do cardápio</h1>
</div>

<?php
   $user_id = $this->session->userdata("user_id");
?>
<form role="form" method="POST" id="form_novo_item" enctype='multipart/form-data'>
   <input type="hidden" name="user_id" value="<?php echo $user_id;?>">
	<div class="form-group">
		<label for="descricao">Descrição do item</label>
		<input required type="text" autocomplete="off" name="descricao" class="form-control" id="descricao">
	</div>
	<div class="form-group">
		<label for="categoria_id">Categoria</label>
		<select class="form-control" required name="categoria_id">
			<option selected disabled >Selecione uma categoria</option>
			<?php
        //carrega as categorias cadastradas no banco, no select
			foreach ($categorias as $categoria){
				echo"<option value='$categoria->categoria_id'>$categoria->nome_categoria</option>";
			}
			?>
		</select>
		<!-- <input required type="" name="categoria" step="0.01" autocomplete="off" class="form-control" id="categoria"> -->
	</div>
	<div class="form-group">
		<label for="valor">Valor</label>
		<input required type="text" name="valor" autocomplete="off" class="form-control" id="valor">
	</div>
<!-- 	<div class="form-group">
		<label for="tempo_producao">Tempo de produção (min)</label>
		<input required type="number" min="1" name="tempo_producao" autocomplete="off" class="form-control" id="tempo_producao">
	</div> -->
	<div class="form-group">
		<label for="file">Imagem</label>
		<span class="file-input btn btn-block btn-primary btn-file">
			Selecione uma imagem <input type="file" name="imagem" id="imagem">
		</span>
	</div>
	<div class="row">
	 <div class="col-md-6">
	  <img id="imgLoaded" style="max-width: 150px;" class="img-responsive">
	</div>
	</div>
	<div class="form-group">
		<label for="ingredientes">Ingredientes</label>
		<textarea required class="form-control" rows="3" name="ingredientes" class="form-control" id="ingredientes"></textarea>
	</div>

	<label for="form-group">Status</label>
	<div class="form-group">
		<label class="radio-inline">
			<input type="radio" name="status" value="a" checked="">Ativo
		</label>
		<label class="radio-inline">
			<input type="radio" name="status" value="i">Inativo
		</label>
	</div>
	<button type="submit" class="btn btn-default btnSalvar">Salvar</button>
	<button  class="btn btn-danger" onclick="window.location='<?php echo base_url("item/listar");?>';return false;">Cancelar</button>
</form>

<script type="text/javascript">
	$(document).ready(function() {
		$('#valor').mask("#.##0,00", {reverse: true});

		  //exibe a imagem carregada
		   function readURL(input) {
		       if (input.files && input.files[0]) {
		           var reader = new FileReader();

		           reader.onload = function (e) {
		               $('#imgLoaded').attr('src', e.target.result);
		           }

		           reader.readAsDataURL(input.files[0]);
		       }
		   }

		   $("#imagem").change(function(){
		       readURL(this);
		   });
		// **************************

		$("#form_novo_item").validate({

			submitHandler:function(form, e) {
				e.preventDefault();
				e.stopPropagation();
				$(".btnSalvar").attr("disabled", "true");

		    	var formData = new FormData(form);

				$.ajax({
					url: '<?php echo base_url("item/salvar");?>',
					type: 'POST',
					mimeType:"multipart/form-data",
					data: formData,
					processData:false,
					contentType: false,
				})
				.always(function(data) {

					var resultado = data.split("|");

					if(resultado[0] == "ok"){
						swal({
						  title: "Item cadastrado com sucesso!",
						  text: "Você deseja cadastrar outro item?",
						  type: "success",
						  showCancelButton: true,
						  confirmButtonColor: "#DD6B55",
						  confirmButtonText: "Sim",
						  cancelButtonText: "Não",
						  closeOnConfirm: false,
						  closeOnCancel: false
						},
						function(isConfirm){
						  if (isConfirm) {
						    window.location.href = "<?php echo base_url('item/cadastrar');?>";
						  } else {
							window.location.href = "<?php echo base_url('item/listar');?>";
						  }
						});
					} else{
						if(resultado[1] =="1062"){
							
							$("#descricao").focus();
							swal({
								title: "Item já cadastrado no banco!",
								type: "error",
								allowOutsideClick: false, 
								allowEscapeKey: false
							}, function(){
								$(".btnSalvar").attr("disabled", false);
							});
						}else{
							$("#descricao").focus();
							swal({
								title:"Erro ao cadastrar no banco de dados!",
								type:"error",
								allowOutsideClick: false, 
								allowEscapeKey: false
							},function(){
								$(".btnSalvar").attr("disabled", false);
							});
						}
					}
				}); //fim $.ajax()

			},
			highlight: function(element) {
				$(element).closest('.form-group').addClass('has-error');

			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
             // errorElement: 'span',
             errorClass: 'help-block',
             errorPlacement: function(error, element) {
             	if(element.parent('.input-group').length) {
             		error.insertAfter(element.parent());
             	} else {
             		error.insertAfter(element);
             	}
             }
      }); //fim $.validate()

   }); //fim document.ready
</script>