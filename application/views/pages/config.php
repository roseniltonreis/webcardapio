<div class="page-header">
	<h2>Configurações</h2>
</div>

<?php

	$usr_logado = $this->session->userdata('user_id');

	
	$result = $this->model_config->get_by_user_id($usr_logado); 

	$query = $result->row();



?>
<form role="form" id="form_config" method="POST">
<input type="hidden" name="user_id" value="<?php echo $this->session->userdata('user_id');?>">
	<div class="form-group">
		<label for="nomeestab">Nome do estabelecimento</label>
		<input required	 type="nomeestab" autocomplete="off" name="nomeestab" class="form-control" id="nomeestab" value="<?php if(isset($query->nome_estabelecimento)) echo($query->nome_estabelecimento);?>">
	</div>
	<label for="form-group">Dias de funcionamento</label>
	<div class="form-group">
		<input required type="text" name="dias_funcionamento" value="<?php if(isset($query->dias_funcionamento)) echo($query->dias_funcionamento);?>" class="form-control" autocomplete="off" id="funcionamento" placeholder="De segunda a sexta das 18:00 às 22:00">
	</div>

	<label for="form-group">Buscar CEP</label>
	<div class="form-group">
		<div class="input-group">
			<input required type="text" name="cep" value="<?php if(isset($query->cep)) echo($query->cep);?>" class="form-control" autocomplete="off" id="cep" placeholder="Buscar CEP">
			<span class="input-group-btn">
				<button class="btn btn-default" id="buscaCep" type="button">Buscar</button>
			</span>
		</div><!-- /input-group -->
	</div><!-- /.col-lg-6 -->

	<div class="endereco" style="display:block;">
		<div class="form-group">
			<label for="rua">Rua</label>
			<input required type="text" name="rua" value="<?php if(isset($query->rua)) echo($query->rua);?>" autocomplete="off" class="form-control" id="rua">
		</div>
		<div class="form-group">
			<label for="numero">Número</label>
			<input type="text" name="numero" value="<?php if(isset($query->numero)) echo($query->numero);?>" autocomplete="off" class="form-control" id="numero">
		</div>
		<div class="form-group">
			<label for="rua">Bairro</label>
			<input required type="text" name="bairro" value="<?php if(isset($query->bairro)) echo($query->bairro);?>" autocomplete="off" class="form-control" id="bairro">
		</div>
		<div class="form-group">
			<label for="rua">Cidade</label>
			<input required type="text" name="cidade" value="<?php if(isset($query->cidade)) echo($query->cidade);?>" autocomplete="off" class="form-control" id="cidade">
		</div>
	</div>
	<button class="btn btn-default">Salvar</button>
	<button  class="btn btn-danger" onclick="window.location='<?php echo base_url("home");?>';return false;">Cancelar</button>
</form>

<script type="text/javascript">

	$( document ).ready(function() {

		$("#form_config").validate({

			submitHandler:function(form, e) {

				e.preventDefault();
				e.stopPropagation();
				$.ajax({
					url: '<?php echo base_url("config/salvar");?>',
					type: 'POST',
					data: $(form).serialize(),
				})
				.always(function(data) {

					if(data == "ok")					
						swal({
							text:'Dados salvos com sucesso!',
							type:'success'
						});

				});
			},
			highlight: function(element) {
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
	        // errorElement: 'span',
	        errorClass: 'help-block',
	        errorPlacement: function(error, element) {
	        	if(element.parent('.input-group').length) {
	        		error.insertAfter(element.parent());
	        	} else {
	        		error.insertAfter(element);
	        	}
	        }
	     });

		// $('.col-sm-10').hide();
		// $(".checkdias").click(function(event) {
		// 	var coluna = $(this).parent().parent().parent().find('.col-sm-10');

		// 	coluna.toggle("fast", function(){
		// 		bool = coluna.css("display") == "none" ? false : true;
		// 		coluna.find("div input").attr("required", bool);
		// 	});
		// // });
		// 	$("form :input").attr("readonly", true);
		// 	$("form :button").attr("readonly", true);

			//masks
			$('#cep').mask('00000-000');
			//masks
			function retornaCep(){
				if($('#cep').val().length == 9)
					$.ajax({
						url: 'http://api.postmon.com.br/v1/cep/'+$('#cep').val().replace("-",""),
				  	// url: 'buscaCep',
				  	type: 'GET',
				  	dataType: 'json'
				  })
				.always(function(data) {

					if(!data.logradouro && !data.bairro){
						$("#rua").attr("readonly", false);
						$("#bairro").attr("readonly", false);
					} else{
						$("#rua").attr("readonly", true);
						$("#bairro").attr("readonly", true);
					}

					$("#rua").val(data.logradouro);
					$("#bairro").val(data.bairro);
					$("#cidade").val(data.cidade);

					if(!$(".endereco").is(":visible")){
						$(".endereco").toggle('slow');
					}
					$("#numero").focus();

				});
				else
					swal('Digite um CEP válido!');
			} //fim retornaCep

			//bloqueia inputs
			$("#rua").attr("readonly", true);
			$("#bairro").attr("readonly", true);
			$("#cidade").attr("readonly", true);


			$("#buscaCep").click(function(){
				retornaCep();
			}); //fim click #buscaCep
			$("#cep").keydown(function(e){
				if( e.which == 13 ){
					e.preventDefault();
					e.stopPropagation();
					retornaCep();
				} 
			});
	}); //fim document.ready
</script>