<div class="page-header">
 <h1>Cadastro de item do cardápio</h1>
</div>

<?php
$idItem = $this->uri->segment(3);
if($idItem == NULL) redirect('item/listar');

$query = $this->item->get_byid($idItem)->row();

$user_id = $this->session->userdata("user_id");
?>
<form role="form" method="POST" id="form_novo_item" enctype='multipart/form-data'>
   <input type="hidden" name="item_id" value="<?php echo $query->item_id;?>">
   <input type="hidden" name="user_id" value="<?php echo $user_id;?>">
   <div class="form-group">
     <label for="descricao">Descrição do item</label>
     <input  type="descricao" autocomplete="off" name="descricao" class="form-control" id="descricao" value="<?php echo $query->descricao;?>">
  </div>
  <div class="form-group">
     <label for="categoria_id">Categoria</label>
     <select class="form-control" name="categoria_id">
      <option disabled >Selecione uma categoria</option>
      <?php
        //carrega as categorias cadastradas no banco, no select
      foreach ($categorias as $categoria){
         $selected = "";
         if($categoria->categoria_id == $query->categoria_id){
            $selected = "selected";
         }
         echo"<option $selected value='$categoria->categoria_id'>$categoria->nome_categoria</option>";
      }
      ?>
   </select>
   <!-- <input required type="" name="categoria" step="0.01" autocomplete="off" class="form-control" id="categoria"> -->
</div>
<div class="form-group">
  <label for="valor">Valor</label>
  <input  type="text" name="valor" step="0.01" autocomplete="off" class="form-control" id="valor" value="<?php echo $query->valor;?>">
</div>
<!-- <div class="form-group">
  <label for="tempo_producao">Tempo de produção (min)</label>
  <input  type="number" min="1" name="tempo_producao" autocomplete="off" class="form-control" id="tempo_producao" value="<?php echo $query->tempo_producao;?>">
</div> -->
<div class="form-group">
  <label for="file">Imagem</label>
  <span class="file-input btn btn-block btn-primary btn-file">
   Selecione uma imagem <input type="file" name="imagem" id="imagem">
</span>
</div>
<div class="row">
 <div class="col-md-6">
 <img id="imgLoaded" style="max-width: 150px;" src="<?php echo(site_url('assets/images/uploads/'.$query->image));?>" class="img-responsive">
</div>
</div>
<div class="form-group">
  <label for="ingredientes">Ingredientes</label>
  <textarea  class="form-control" rows="3" name="ingredientes" class="form-control" id="ingredientes"><?php echo $query->ingredientes;?></textarea>
</div>

<label for="form-group">Status</label>
<div class="form-group">
  <label class="radio-inline">
   <input type="radio" name="status" value="a" checked="">Ativo
</label>
<label class="radio-inline">
   <input type="radio" name="status" value="i">Inativo
</label>
</div>
<button type="submit" class="btn btn-default btnAtualizar">Atualizar</button>
<button  class="btn btn-danger" onclick="window.location='<?php echo base_url("item/listar");?>';return false;">Cancelar</button>
</form>

<script type="text/javascript">
 $(document).ready(function() {
  $('#valor').mask("#.##0,00", {reverse: true});


  //exibe a imagem carregada
  function readURL(input) {
    if (input.files && input.files[0]) {
     var reader = new FileReader();

     reader.onload = function (e) {
      $('#imgLoaded').attr('src', e.target.result);
   }

   reader.readAsDataURL(input.files[0]);
}
}

$("#imagem").change(function(){
 readURL(this);
});
// **************************


$("#form_novo_item").validate({

   submitHandler:function(form, e) {
    e.preventDefault();
    e.stopPropagation();
    $(".btnAtualizar").attr("disabled", true);

    var formData = new FormData(form);

    $.ajax({
     url: '<?php echo base_url("item/atualizar");?>',
     type: 'POST',
     mimeType:"multipart/form-data",
     data: formData,
     processData:false,
     contentType: false,
  })
    .always(function(data) {

     var resultado = data;

     if(resultado == "ok"){
      swal({
        title: "Item alterado com sucesso!",
        type: "success",
        allowOutsideClick: false, 
        allowEscapeKey: false
    },function(){
       window.location.href = "<?php echo base_url('item/listar');?>";
    });
   } else{
    $(".btnAtualizar").attr("disabled", false);
    toastr["error"]("Por favor, verifique se todos os campos foram preenchidos corretamente!", "Erro!")
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-bottom-full-width",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }//fim toastr
    //   if(resultado[1] =="1062"){
    //    $("#descricao").focus();
    //    swal({
    //     title: "Item já cadastrado no banco!",
    //     type: "error"
    //  }, function(){
    //  });
    // }else{
    //    $("#descricao").focus();
    //    swal({
    //     title:"Erro ao cadastrar no banco de dados!",
    //     type:"error"
    //  },function(){
    //  });
    // }
  }//fim else
        }); //fim $.ajax()

},
highlight: function(element) {
 $(element).closest('.form-group').addClass('has-error');
},
unhighlight: function(element) {
 $(element).closest('.form-group').removeClass('has-error');
},
             // errorElement: 'span',
             errorClass: 'help-block',
             errorPlacement: function(error, element) {
              if(element.parent('.input-group').length) {
               error.insertAfter(element.parent());
            } else {
               error.insertAfter(element);
            }
         }
      }); //fim $.validate()

   }); //fim document.ready
</script>