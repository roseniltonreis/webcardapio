<div class="page-header">
	<h1>Cadastro de Categorias</h1>
</div>


<?php
$idCategoria = $this->uri->segment(3);
if($idCategoria == NULL) redirect('categoria/listar');

$query = $this->categoria->get_byid($idCategoria)->row();

?>

<form role="form" method="POST" id="form_categoria">
	<input type="hidden" name="idcategoria" value="<?php echo $idCategoria;?>">
	<div class="form-group">
		<label for="nome_categoria">Nome da categoria</label>
		<input type="text" required autocomplete="off" name="nome_categoria" value="<?php echo $query->nome_categoria;?>" class="form-control">
	</div>

	<button type="submit" class="btn btn-default">Salvar</button>
	<button  class="btn btn-danger" onclick="window.location='<?php echo base_url("categoria/listar");?>';return false;">Cancelar</button>
</form>


<script type="text/javascript">

	$( document ).ready(function() {

		$("#form_categoria").validate({

			submitHandler:function(form, e) {
				e.preventDefault();
				e.stopPropagation();

				$.ajax({
					url: '<?php echo base_url("categoria/updateCategoria");?>',
					type: 'POST',
					data: $(form).serialize(),
				})
				.always(function(data) {
					console.log(data);
					if(data == "ok"){
						swal({
							title: "Dados alterados com sucesso!",
							type: "success"
						},function(){
							window.location.href = "<?php echo base_url('categoria/listar');?>";
						});
					} else{
						swal("Erro ao cadastrar no banco de dados!");
					}
				}); //fim $.ajax()

			},
			highlight: function(element) {
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
          // errorElement: 'span',
          errorClass: 'help-block',
          errorPlacement: function(error, element) {
          	if(element.parent('.input-group').length) {
          		error.insertAfter(element.parent());
          	} else {
          		error.insertAfter(element);
          	}
          }
   }); //fim $.ajax()

  }); //fim document.ready
</script>