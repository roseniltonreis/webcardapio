<div class="page-header">
	<h2>Configurações</h2>
</div>

<?php

	$query = $this->model_config->get_byid(11)->row();

	$dias_funcionamento = unserialize($query->dias_funcionamento);

	$obj_dias_funcionamento = (object) $dias_funcionamento;

	// var_dump($obj_dias_funcionamento);

	// echo($obj_dias_funcionamento->segunda["hora_inicio"]);


	$check_segunda = $inicio_segunda = $fim_segunda = $check_terca = $inicio_terca = $fim_terca = $check_quarta = 
	$inicio_quarta = $fim_quarta = $check_quinta = $inicio_quinta = $fim_quinta = $check_sexta = $inicio_sexta = 
	$fim_sexta = $check_sabado = $inicio_sabado = $fim_sabado = $check_domingo = $inicio_domingo = $fim_domingo = "";


	if(!empty($obj_dias_funcionamento->segunda)){
		$check_segunda = "checked";
		$inicio_segunda = $obj_dias_funcionamento->segunda["hora_inicio"];
		$fim_segunda = $obj_dias_funcionamento->segunda["hora_fim"];
	}
	if(!empty($obj_dias_funcionamento->terca)){
		$check_terca = "checked";
		$inicio_terca = $obj_dias_funcionamento->terca["hora_inicio"];
		$fim_terca = $obj_dias_funcionamento->terca["hora_fim"];
	}
	if(!empty($obj_dias_funcionamento->quarta)){
		$check_quarta = "checked";
		$inicio_quarta = $obj_dias_funcionamento->quarta["hora_inicio"];
		$fim_quarta = $obj_dias_funcionamento->quarta["hora_fim"];
	}
	if(!empty($obj_dias_funcionamento->quinta)){
		$check_quinta = "checked";
		$inicio_quinta = $obj_dias_funcionamento->quinta["hora_inicio"];
		$fim_quinta = $obj_dias_funcionamento->quinta["hora_fim"];
	}
	if(!empty($obj_dias_funcionamento->sexta)){
		$check_sexta = "checked";
		$inicio_sexta = $obj_dias_funcionamento->sexta["hora_inicio"];
		$fim_sexta = $obj_dias_funcionamento->sexta["hora_fim"];
	}
	if(!empty($obj_dias_funcionamento->sabado)){
		$check_sabado = "checked";
		$inicio_sabado = $obj_dias_funcionamento->sabado["hora_inicio"];
		$fim_sabado = $obj_dias_funcionamento->sabado["hora_fim"];
	}
	if(!empty($obj_dias_funcionamento->domingo)){
		$check_domingo = "checked";
		$inicio_domingo = $obj_dias_funcionamento->domingo["hora_inicio"];
		$fim_domingo = $obj_dias_funcionamento->domingo["hora_fim"];
	}

?>
<form role="form" id="form_config" method="POST">
<input type="hidden" name="user_id" value="<?php echo $this->session->userdata('user_id');?>">
	<div class="form-group">
		<label for="nomeestab">Nome do estabelecimento</label>
		<input required	 type="nomeestab" autocomplete="off" name="nomeestab" class="form-control" id="nomeestab" value="<?php echo $query->nome_estabelecimento;?>">
	</div>
	<label for="form-group">Dias de funcionamento</label>
	<div class="form-group">
		<div class="row">
			<div class="col-sm-2">
				<label class="checkbox alignright"><input type="checkbox" <?=$check_segunda;?> class="checkdias" value="1" name="dia[segunda]">segunda</label>
			</div>
			<div class="col-sm-10">
				<div class="col-sm-5">
					<input type="text" id="hr_ini_segunda"name="hr_ini_segunda" value="<?=$inicio_segunda;?>" placeholder="Horário abertura (00:00)" autocomplete="off" class="form-control">
				</div>
				<div class="col-sm-5">
					<input	 type="text" id="hr_fim_segunda"name="hr_fim_segunda" value="<?=$fim_segunda;?>" placeholder="Horário fechamento (00:00)" autocomplete="off" class="form-control">
				</div>
				<div class="col-sm-offset-2"></div>
			</div>	
		</div>
		<div class="row">
			<div class="col-sm-2">
				<label class="checkbox alignright"><input type="checkbox" <?=$check_terca?> class="checkdias" value="1" name="dia[terca]">terça</label>
			</div>
			<div class="col-sm-10">
				<div class="col-sm-5">
					<input type="text" id="hr_ini_terca"name="hr_ini_terca" value="<?=$inicio_terca;?>" placeholder="Horário abertura (00:00)" autocomplete="off" class="form-control">
				</div>
				<div class="col-sm-5">
					<input type="text" id="hr_fim_terca"name="hr_fim_terca" value="<?=$fim_terca;?>" placeholder="Horário fechamento (00:00)" autocomplete="off" class="form-control">
				</div>
				<div class="col-sm-offset-2"></div>	
			</div>
		</div>
		<div class="row">
			<div class="col-sm-2">
				<label class="checkbox alignright"><input type="checkbox" <?=$check_quarta;?> class="checkdias" value="1" name="dia[quarta]">quarta</label>
			</div>
			<div class="col-sm-10">
				<div class="col-sm-5">
					<input type="text" id="hr_ini_quarta"name="hr_ini_quarta" value="<?=$inicio_quarta;?>" placeholder="Horário abertura (00:00)" autocomplete="off" class="form-control">
				</div>
				<div class="col-sm-5">
					<input type="text" id="hr_fim_quarta"name="hr_fim_quarta" value="<?=$fim_quarta;?>" placeholder="Horário fechamento (00:00)" autocomplete="off" class="form-control">
				</div>
				<div class="col-sm-offset-2"></div>	
			</div>
		</div>
		<div class="row">
			<div class="col-sm-2">
				<label class="checkbox alignright"><input type="checkbox" <?=$check_quinta;?> class="checkdias" value="1" name="dia[quinta]">quinta</label>
			</div>
			<div class="col-sm-10">
				<div class="col-sm-5">
					<input type="text" id="hr_ini_quinta"name="hr_ini_quinta" value="<?=$inicio_quinta;?>" placeholder="Horário abertura (00:00)" autocomplete="off" class="form-control">
				</div>
				<div class="col-sm-5">
					<input	 type="text" id="hr_fim_quinta"name="hr_fim_quinta" value="<?=$fim_quinta;?>" placeholder="Horário fechamento (00:00)" autocomplete="off" class="form-control">
				</div>
				<div class="col-sm-offset-2"></div>	
			</div>
		</div>
		<div class="row">
			<div class="col-sm-2">
				<label class="checkbox alignright"><input type="checkbox" <?=$check_sexta;?> class="checkdias" value="1" name="dia[sexta]">sexta</label>
			</div>
			<div class="col-sm-10">
				<div class="col-sm-5">
					<input type="text" id="hr_ini_sexta"name="hr_ini_sexta" value="<?=$inicio_sexta;?>" placeholder="Horário abertura (00:00)" autocomplete="off" class="form-control">
				</div>
				<div class="col-sm-5">
					<input type="text" id="hr_fim_sexta"name="hr_fim_sexta" value="<?=$fim_sexta;?>" placeholder="Horário fechamento (00:00)" autocomplete="off" class="form-control">
				</div>
				<div class="col-sm-offset-2"></div>	
			</div>
		</div>
		<div class="row">
			<div class="col-sm-2">
				<label class="checkbox alignright"><input type="checkbox" <?=$check_sabado;?> class="checkdias" value="1" name="dia[sabado]">sábado</label>
			</div>
			<div class="col-sm-10">
				<div class="col-sm-5">
					<input type="text" id="hr_ini_sabado"name="hr_ini_sabado" value="<?=$inicio_sabado;?>" placeholder="Horário abertura (00:00)" autocomplete="off" class="form-control">
				</div>
				<div class="col-sm-5">
					<input type="text" id="hr_fim_sabado"name="hr_fim_sabado" value="<?=$fim_sabado;?>" placeholder="Horário fechamento (00:00)" autocomplete="off" class="form-control">
				</div>
				<div class="col-sm-offset-2"></div>	
			</div>
		</div>
		<div class="row">
			<div class="col-sm-2">
				<label class="checkbox alignright"><input type="checkbox" <?=$check_domingo;?> class="checkdias" value="1" name="dia[domingo]">domingo</label>
			</div>
			<div class="col-sm-10">
				<div class="col-sm-5">
					<input type="text" id="hr_ini_domingo"name="hr_ini_domingo" value="<?=$inicio_domingo;?>" placeholder="Horário abertura (00:00)" autocomplete="off" class="form-control">
				</div>
				<div class="col-sm-5">
					<input type="text" id="hr_fim_domingo"name="hr_fim_domingo" value="<?=$fim_domingo;?>" placeholder="Horário fechamento (00:00)" autocomplete="off" class="form-control">
				</div>
				<div class="col-sm-offset-2"></div>
			</div>
		</div>

	</div>

	<label for="form-group">Buscar CEP</label>
	<div class="form-group">
		<div class="input-group">
			<input required type="text" name="cep" value="<?=$query->cep?>" class="form-control" autocomplete="off" id="cep" placeholder="Buscar CEP">
			<span class="input-group-btn">
				<button class="btn btn-default" id="buscaCep" type="button">Buscar</button>
			</span>
		</div><!-- /input-group -->
	</div><!-- /.col-lg-6 -->

	<div class="endereco" style="display:block;">
		<div class="form-group">
			<label for="rua">Rua</label>
			<input required type="text" name="rua" value="<?=$query->rua?>" autocomplete="off" class="form-control" id="rua">
		</div>
		<div class="form-group">
			<label for="numero">Número</label>
			<input type="text" name="numero" value="<?=$query->numero?>" autocomplete="off" class="form-control" id="numero">
		</div>
		<div class="form-group">
			<label for="rua">Bairro</label>
			<input required type="text" name="bairro" value="<?=$query->bairro?>" autocomplete="off" class="form-control" id="bairro">
		</div>
		<div class="form-group">
			<label for="rua">Cidade</label>
			<input required type="text" name="cidade" value="<?=$query->cidade?>" autocomplete="off" class="form-control" id="cidade">
		</div>
	</div>
	<button class="btn btn-default">Salvar</button>
	<button  class="btn btn-danger" onclick="window.location='<?php echo base_url("categoria/listar");?>';return false;">Cancelar</button>
</form>
<button class="btn btn-success btn-block">Editar</button>

<script type="text/javascript">

	$( document ).ready(function() {

		$("#form_config").validate({

			submitHandler:function(form, e) {

				e.preventDefault();
				e.stopPropagation();
				$.ajax({
					url: '<?php echo base_url("config/salvar");?>',
					type: 'POST',
					data: $(form).serialize(),
				})
				.always(function(data) {
					console.log(data);

				});
			},
			highlight: function(element) {
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
	        // errorElement: 'span',
	        errorClass: 'help-block',
	        errorPlacement: function(error, element) {
	        	if(element.parent('.input-group').length) {
	        		error.insertAfter(element.parent());
	        	} else {
	        		error.insertAfter(element);
	        	}
	        }
	     });

		// $('.col-sm-10').hide();
		// $(".checkdias").click(function(event) {
		// 	var coluna = $(this).parent().parent().parent().find('.col-sm-10');

		// 	coluna.toggle("fast", function(){
		// 		bool = coluna.css("display") == "none" ? false : true;
		// 		coluna.find("div input").attr("required", bool);
		// 	});
		// });
			$("form :input").attr("readonly", true);
			$("form :button").attr("readonly", true);

			//masks
			$('#cep').mask('00000-000');
			$('#hr_ini_segunda').mask('00:00');
			$('#hr_fim_segunda').mask('00:00');
			$('#hr_ini_terca').mask('00:00');
			$('#hr_fim_terca').mask('00:00');
			$('#hr_ini_quarta').mask('00:00');
			$('#hr_fim_quarta').mask('00:00');
			$('#hr_ini_quinta').mask('00:00');
			$('#hr_fim_quinta').mask('00:00');
			$('#hr_ini_sexta').mask('00:00');
			$('#hr_fim_sexta').mask('00:00');
			$('#hr_ini_sabado').mask('00:00');
			$('#hr_fim_sabado').mask('00:00');
			$('#hr_ini_domingo').mask('00:00');
			$('#hr_fim_domingo').mask('00:00');
			//masks
			function retornaCep(){
				if($('#cep').val().length == 9)
					$.ajax({
						url: 'http://api.postmon.com.br/v1/cep/'+$('#cep').val().replace("-",""),
				  	// url: 'buscaCep',
				  	type: 'GET',
				  	dataType: 'json'
				  })
				.always(function(data) {

					if(!data.logradouro && !data.bairro){
						$("#rua").attr("readonly", false);
						$("#bairro").attr("readonly", false);
					} else{
						$("#rua").attr("readonly", true);
						$("#bairro").attr("readonly", true);
					}

					$("#rua").val(data.logradouro);
					$("#bairro").val(data.bairro);
					$("#cidade").val(data.cidade);

					if(!$(".endereco").is(":visible")){
						$(".endereco").toggle('slow');
					}
					$("#numero").focus();

				});
				else
					swal('Digite um CEP válido!');
			} //fim retornaCep

			//bloqueia inputs
			$("#rua").attr("readonly", true);
			$("#bairro").attr("readonly", true);
			$("#cidade").attr("readonly", true);


			$("#buscaCep").click(function(){
				retornaCep();
			}); //fim click #buscaCep
			$("#cep").keydown(function(e){
				if( e.which == 13 ){
					e.preventDefault();
					e.stopPropagation();
					retornaCep();
				} 
			});
	}); //fim document.ready
</script>