  <?php
		// $convertido = json_decode($itens);

		// $data = json_encode((array)$itens);
		// var_dump($data);

		// $sql = "SELECT *FROM item";
		// $result = mysqli_query($connection, $sql);
  $contador = 0;
  $linha = "<div class='row'>";
  echo $linha;

  foreach ($itens as $obj) {
  	echo "<div style='padding-bottom:30px;' class='col-md-12 col-sm-12 col-xs-12 col-lg-12' id='coluna_item_".$obj->item_id."'>";
  	
  	if($obj->image != ""){
  		$caminhoImagem = site_url('assets/images/uploads/'.$obj->image);
  	} else{
  		$caminhoImagem = site_url('assets/images/no-img.png');
  	}

  	?>
  	<div class="card">
  		<!-- <div class="mask"></div> -->
  		<div class="image">
  			<img src="<?php echo $caminhoImagem;?>" alt="">
  		</div>
  		<div class="tituloitem">
  			<p><?php echo $obj->descricao;?></p>
  		</div>
  		<div class="conteudoitem">
  		<?=$obj->ingredientes?>
  		</div>
  		<div class="container">
  			<div class="row bottomactions vertical-align">
  				<div class="col-sm-4 col-xs-4 btn btn-success" onclick="window.location='<?php echo base_url('item/editar/'.$obj->item_id);?>';return false;">
  					Editar
  				</div>

  				<div class="col-sm-4 col-xs-4 btn btn-danger" onclick="deletarItem(<?php echo $obj->item_id;?>);">
  					Deletar
  				</div>
  				<div class="col-sm-4 col-xs-4">
  					<p class="tempo"><?php echo $obj->status == "i"? "Inativo" : "Ativo";?></p>
  				</div>
<!--   				<div class="col-sm-3 col-xs-3">
  					<p class="tempo"><?=$obj->tempo_producao;?> <span style="font-size:10px;">min</span></p>
  				</div> -->
  				<div class="col-sm-4 col-xs-4">
  					<p class="preco"><span style="font-size:10px;">R$</span><?php echo str_replace('.', ',', $obj->valor);?></p>
  				</div>
  			</div>
  		</div>
  	</div>
  </div> <!-- fecha coluna -->


  <?php

  $contador++;
  if($contador%3 == 0) echo '</div><div class="row">';
} ?>
</div>