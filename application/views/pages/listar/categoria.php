

<div class="page-header">
  <h1>Categorias cadastradas</h1>
  <!-- <button class="btn btn-block btn-primary">Nova categoria</button> -->
  <?php echo anchor('categoria/cadastrar','Nova categoria', array('class' => 'btn btn-block btn-primary'));?>
</div>
<?php
$tmpl = array (
  'table_open' => '<table class="table table-hover table-responsive table-condensed">',

  'heading_row_start'   => '<tr>',
  'heading_row_end'     => '</tr>',
  'heading_cell_start'  => '<th>',
  'heading_cell_end'    => '</th>',

  'row_start'           => '<tr>',
  'row_end'             => '</tr>',
  'cell_start'          => '<td>',
  'cell_end'            => '</td>',

  'row_alt_start'       => '<tr>',
  'row_alt_end'         => '</tr>',
  'cell_alt_start'      => '<td>',
  'cell_alt_end'        => '</td>',

  'table_close'         => '</table>'
  );
$this->table->set_template($tmpl);

$this->table->set_heading('Id', 'Nome da categoria', 'Ações');
foreach ($categorias as $categoria):
$this->table->add_row(
  $categoria->categoria_id,
  $categoria->nome_categoria,
  anchor("categoria/editar/".$categoria->categoria_id,'<i class="fa fa-pencil"></i>')." <a href='#' class='deletar' data-value='".$categoria->categoria_id."'><i class='fa fa-trash-o'></i></a>"
  );
endforeach;
echo $this->table->generate();
?>

<script type="text/javascript">
  $(".deletar").click(function(event) {
   event.preventDefault();
   event.stopPropagation();

   var idCategoria = $(this).attr("data-value");

   // swal({
   //    title: 'Deletar categoria!',
   //    showCancelButton: true,
   //    confirmButtonText: 'Submit',
   //    closeOnConfirm: false }, 
   //    function() {
   //       swal.disableButtons();
   //       setTimeout(function() {
   //       swal('Ajax request finished!');   
   //    }, 2000); 
   // });


  swal({
     title: 'Deletar categoria!',
     text: 'Esta operação não pode ser desfeita e pode \
     afetar registros que usarem esta categoria, tem certeza que deseja deletar?',
     type: 'warning',
     showCancelButton: true,
     confirmButtonColor: '#3085d6',
     cancelButtonColor: '#d33',
     confirmButtonText: 'Sim, Deletar!',
     cancelButtonText: 'Cancelar',
     closeOnConfirm: false 
   }, function() {
      swal.disableButtons();

      $.ajax({
         url: 'excluir',
         type: 'POST',
         data: {id: idCategoria},
      })
      .always(function(data) {
         
         if(data == "ok"){
            swal.enableButtons();
            swal({
               text:'Categoria deletada com sucesso!',
               type: 'success'
            }, function(){
               window.location.href = "<?php echo base_url('categoria/listar');?>";
            });  
         }
      }); //fim always
   }); //fim function

});
</script>