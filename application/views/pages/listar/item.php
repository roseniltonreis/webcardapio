

<link rel="stylesheet" href="//cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">
<script type="text/javascript" src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>

<div class="page-header">
	<h1>Itens cadastrados</h1>
  <?php echo anchor('item/cadastrar','Novo item', array('class' => 'btn btn-block btn-primary'));?>

</div>

<table id="tbl_itens" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Imagem</th>
                <th>Descrição</th>
                <th>valor</th>
                <th>Categoria</th>
                <th>Status</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Imagem</th>
                <th>Descrição</th>
                <th>valor</th>
                <th>Categoria</th>
                <th>Status</th>
                <th>Ações</th>
            </tr>
        </tfoot>
        <tbody>
            <!-- <tr>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>2011/04/25</td>
            </tr>
            <tr>
                <td>Garrett Winters</td>
                <td>Accountant</td>
                <td>Tokyo</td>
                <td>63</td>
                <td>2011/07/25</td>
            </tr> -->
    	</tbody>
</table>

	<script>

	$(document).ready(function() {
    	var table = $('#tbl_itens').DataTable({
    		"oLanguage": {
				"oPaginate": {
			        "sFirst": "Primeira Página",
			        "sNext": 'Próxima',
			        'sPrevious': 'Anterior',
			        'sLast': 'Ultima Página'
			    },
			    "oAria": {
			        "sSortAscending": " - clique para ordem crescente",
			        "sSortDescending": " - clique para ordem decrescente"
			    },
				"sSearch": "Pesquisar:",
			    "sEmptyTable": "Nenhum produto cadastrado",
			    "sInfo": "Mostrando _START_ de _END_ de um total de _TOTAL_ registros",
			    "sInfoEmpty": "Mostrando 0 de 0 de um total de 0 registros",
			    "sInfoFiltered": " - filtrado de um total de _MAX_ registros",
			    "sLengthMenu": "Mostrar _MENU_ registros",
			    "sLoadingRecords": "Carregando! Por favor aguarde ...",
			    "sProcessing": "A tabela está sendo montada ...",
			    "sZeroRecords": "Nenhum registro encontrado!"
			},
			"responsive":true,
		   "ajax": {
		      "url": "<?php echo base_url('item/get_itens');?>",
		      "dataSrc": "item"
		   },
		   "columns": [
		      {
		      	"render":function(data, type, full, meta){
		      		return '<img src="<?php echo base_url("assets/images/uploads/");?>'+"/"+full.image+'" alt="Imagem '+full.descricao+'">';
		      	}
		      },
		      { "data": "descricao" },
		      { "data": "valor" },
		      { "data": "nome_categoria" },
		      { 
		      	"render":function(data, type, full, meta){
		      		var status = "";
		      		if(full.status == "a"){
		      			status = "ativo";
		      		}else{
		      			status = "inativo";
		      		}
		      		return status;
		      	}
		      },
		      {
					"orderable": true,
					"render": function ( data, type, full, meta ) {
						// console.log("Kenzes");
						// console.log("type: "+type);
						// console.log(meta);
                     id = full.id;
                     return '<div class="btn-group" role="group" aria-label="Basic example">\
										  <a type="button" class="btn btn-success" href="<?php echo base_url("item/editar/");?>/'+id+'">Editar</a>\
										  <a type="button" class="btn btn-danger deleteBtn" data-item-id="'+id+'">Deletar</a>\
										</div>';
                 }
				}
		   ],
		   "order": [[1, 'asc']]
	    });


		$("#tbl_itens tbody").on( 'click', 'tr .deleteBtn', function () {

			urlDel = "<?php echo base_url('item/excluir');?>";
			tr = $(this).parent().parent().parent();
			linha = $(this).parent().parent().parent();
			item_id = $(this).data("item-id");

		   	swal({
				title: 'Tem certeza que deseja deletar este item?',
				text: 'Esta ação não poderá ser desfeita!',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim',
				cancelButtonText: 'Não',
				allowOutsideClick: false,
				allowEscapeKey: false,
				closeOnConfirm: false
			}, function() {
				swal.disableButtons();
				  $.ajax({
				  	url: urlDel,
				  	type: 'POST',
				  	data: {"id": item_id},
				  })
				  .always(function(data) {

				  	if(data == "ok"){
					  	swal({
							title: 'Deletado!',
							text: 'Item deletado com sucesso!',
							type: 'success'
						}, function(){
							//executa fade e remove a linha
							linha.fadeOut(500, function() {
								$(this).remove();
							});
						});
				  	} else{
				  		swal({
							title: 'Erro!',
							text: 'Erro ao deletar item, contacte um administrador do sistema!',
							type: 'error'
						});
				  	}
				  });
			});
		} );

	});//fim: doc.ready
	</script>

<style type="text/css">
	*{
		margin:0;
		padding:0;
	}
	.vertical-align {
		display: flex;
		align-items: center;
	}
	.bordercol{
		border-right: 1px solid;
	}
	.mask{
		width:100%;
		height:100%;
		position:absolute;
		background:rgba(146, 146, 146, 0.47);
		pointer-events: none;
		z-index:300;
	}

	.tituloitem p{
		margin:0;
		padding:0;
		text-align: center;
		position:relative;
		font-size:23px;
		text-align:center;
		margin-top: 4px;
		font-weight:bold;
		color: #971c1c;
	}

	.conteudoitem{
		position:relative;
		padding:16px;
		text-align:justify;
		color:#22223b;
	}

	.preco{
		padding:0;
		margin:0;
		font-size: 18px;
		color:#a03030;
		font-weight:bold;
	}

	.tempo{
		padding:0;
		margin:0;
		font-size: 18px;
		color:#5155E0;
		font-weight:bold;
	}
	
	.bottomactions{
		position:absolute;
		width:100%;
		bottom:0;
		text-align:center;
		font-size: 22px;
	}
	.card{
		position:relative;
		min-width: 300px;
		height:180px;
		padding-bottom: 30px;
		border: 1px solid #D6CECE;
		background: white;
/*		-webkit-box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 1);
		-moz-box-shadow:    2px 2px 4px 1px rgba(0, 0, 0, 1);
		box-shadow:		    2px 2px 4px 1px rgba(0, 0, 0, 1);*/
	}
	.image{
		position:relative;
		width:150px;
		float:left;
		padding: 8px;
	}
	img {
		width: 100%;
		margin: 0 auto;
		display: block;
	}

</style>