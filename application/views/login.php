<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="<?php echo(base_url("assets/css/stylelogin.css"));?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/sweetalert2.css"); ?>">
    <link rel="icon" href="<?php echo base_url('assets/images/favicon.ico');?>">
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url("assets/js/sweetalert2.min.js"); ?>"></script>
</head>
<body>
    <?php

    if($this->session->flashdata('mensagem')){
        echo '<script> swal({title:"Erro",text:"'.$this->session->flashdata("mensagem").'", type:"error"},
            function(){$("#login").focus();});</script>';
    }

        // if(!$this->session->userdata('usuario_logado') ){
        //     echo '<script> swal("Erro","Usuário ou senha, inválidos!", "error");</script>';
        // }

    ?>

        <div class="logindiv">
            <div class="modal-dialog">
                <div class="loginmodal-container">
                    <h1>Login</h1><br>
                  <form method="POST" action="<?php echo base_url("login/autenticar"); ?>">
                    <input type="text" name="login" placeholder="Usuário" id="login">
                    <input type="password" name="senha" placeholder="Senha">
                    <input type="submit" class="login loginmodal-submit" value="Login">
                  </form>
                </div>
            </div>
        </div>

</body>
</html>
