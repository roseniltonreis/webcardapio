<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url('assets/images/favicon.ico');?>">

    <title><?php echo $titulo;?></title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="<?php echo(base_url("assets/css/sticky-footer-navbar.css"));?>">
    <link rel="stylesheet" href="<?php echo(base_url("assets/css/custom.css"));?>">
    <link rel="stylesheet" href="<?php echo(base_url("assets/css/fileinput.min.css"));?>">
    <link rel="stylesheet" href="<?php echo(base_url("assets/css/sweetalert2.css"));?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Custom styles for this template -->
    <link href="<?php echo(base_url("assets/css/sticky-footer-navbar.css"));?>" rel="stylesheet">

    <!-- Jquery lib -->
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        <?php echo anchor('home','Dev3 Solutions', array('class' => 'navbar-brand'));?>

        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><?php echo anchor('home', 'Home');?></li>
               <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Gerenciar cardápio<span class="caret"></span></a>
                <ul class="dropdown-menu">
                <li class="dropdown-header">Item<hr></li>
                    <li><?php echo anchor('item/listar','Listar itens');?></li>
                    <li><?php echo anchor('item/cadastrar','Novo item');?></li>
                    <!-- <li role="separator" class="divider"></li> -->
                <li class="dropdown-header">Categorias<hr></li>
                    <li><?php echo anchor('categoria/listar','Listar categorias');?></li>
                    <li><?php echo anchor('categoria/cadastrar','Nova categoria');?></li>

                </ul>
            </li>
                <li><?php echo anchor('localizacao','Localização');?></li>
           
            <!-- <li><?php echo anchor('config','Configurações');?></li> -->
          </ul>
          <ul class="nav navbar-nav navbar-right">
               <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo($this->session->userdata('user'));?><span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><?php echo anchor('login/logout', 'Logout');?></li>
                <!-- <li><a href="#">Logout</a></li> -->
                <li><?php echo anchor('config','Configurações');?></li>
              </ul>
            </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <!-- Begin page content -->
    <div class="container">