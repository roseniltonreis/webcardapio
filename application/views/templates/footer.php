    </div> <!-- Fecha div container -->

    <footer class="footer">
      <div class="container">
        <p class="text-muted">Dev3 2015 - Todos os direitos reservados</p>
      </div>
    </footer>
    <link rel="stylesheet" href="<?php echo(base_url("assets/css/fileinput.min.css"));?>">


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="<?php echo(base_url("assets/js/fileinput.min.js"));?>"></script>
    <script src="<?php echo(base_url("assets/js/bootstrap-select.js"));?>"></script>
    <script src="<?php echo(base_url("assets/js/sweetalert2.min.js"));?>"></script>
    <script src="<?php echo(base_url("assets/js/jquery.validate.min.js"));?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>
  </body>
</html>