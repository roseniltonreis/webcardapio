<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_config extends CI_Model{

	public function do_insert($dados=NULL){

		if($dados != NULL):
			$this->db->insert('config', $dados);
			// $this->session->set_flashdata('cadastrook', 'Cadastro efetuado com sucesso');
		endif;
	}

	public function do_update($dados=NULL, $condicao=NULL){
		if($dados != NULL && $condicao != NULL):
			$this->db->update('config', $dados, $condicao);
		endif;
	}
	public function get_all(){
		return $this->db->get('item');
	}

	public function get_byid($id=''){
		if($id != NULL):
			$this->db->where('config_id', $id);
			$this->db->limit(1);
			return $this->db->get("config");
		else:
			return FALSE;
		endif;
	}

	public function get_by_user_id($id=''){
		if($id != NULL):
			$this->db->where('user_id', $id);
			$this->db->limit(1);
			return $this->db->get("config");
		else:
			return FALSE;
		endif;
	}

	public function do_delete($condicao=NULL){
		if($condicao!=NULL):
			$this->db->delete("cliente", $condicao);
		$this->session->set_flashdata("excluirok", "ok");
		// redirect("cliente/listar");
		endif;
	}

}