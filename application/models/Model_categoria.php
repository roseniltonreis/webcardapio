<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_categoria extends CI_Model{

	public function do_insert($dados=NULL){

		if($dados != NULL):
			$this->db->insert('categoria', $dados);
		endif;
	}

	public function do_update($dados=NULL, $condicao=NULL){
		if($dados != NULL && $condicao != NULL):
			$this->db->update('categoria', $dados, $condicao);
			return $this->db->error();
		endif;
	}
	public function get_all(){
		$this->db->order_by("categoria_id", "asc");
		return $this->db->get_where('categoria', array('user_id' => $this->session->userdata("user_id")));
	}

	public function category_exists($category_name=""){
		return $this->db->get_where('categoria', array('nome_categoria' => $category_name,
			'user_id' => $this->session->userdata('user_id')));
	}

	public function get_byid($id=''){
		if($id != NULL):
			$this->db->where('categoria_id', $id);
			$this->db->limit(1);
			return $this->db->get("categoria");
		else:
			return FALSE;
		endif;
	}

	public function do_delete($condicao=NULL){
		if($condicao!=NULL):
			$this->db->delete("categoria", $condicao);
			return $this->db->affected_rows();
		endif;
	}
}