<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_cardapio extends CI_Model{

	public function do_insert($dados=NULL){

		if($dados != NULL):
			$this->db->insert('item', $dados);
			return $this->db->error();
			// $this->session->set_flashdata('cadastrook', 'Cadastro efetuado com sucesso');
		endif;
	}

	public function do_update($dados=NULL, $condicao=NULL){
		if($dados != NULL && $condicao != NULL):
			$this->db->update('item', $dados, $condicao);
			// $this->session->set_flashdata('edicaook', 'Cadastro alterado com sucesso');
			// redirect(current_url());
		endif;
	}
	public function get_all(){
		// return $this->db->get('item');
		return $this->db->get_where('item', array('user_id' => $this->session->userdata("user_id")));
	}

	public function get_byid($id=''){
		if($id != NULL):
			$this->db->where('item_id', $id);
			$this->db->limit(1);
			return $this->db->get("item");
		else:
			return FALSE;
		endif;
	}

	public function do_delete($condicao=NULL){
		if($condicao!=NULL):
			$this->db->delete("cliente", $condicao);
		$this->session->set_flashdata("excluirok", "ok");
		// redirect("cliente/listar");
		endif;
	}

}