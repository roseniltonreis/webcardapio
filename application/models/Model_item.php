<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_item extends CI_Model{

	public function do_insert($dados=NULL){

		if($dados != NULL):
			$this->db->insert('item', $dados);
			return $this->db->error();
			// $this->session->set_flashdata('cadastrook', 'Cadastro efetuado com sucesso');
		endif;
	}

	public function do_update($dados=NULL, $condicao=NULL){
		if($dados != NULL && $condicao != NULL):
			$this->db->update('item', $dados, $condicao);
			// $this->session->set_flashdata('edicaook', 'Cadastro alterado com sucesso');
			// redirect(current_url());
		endif;
	}
	public function get_all(){
		// return $this->db->get('item');
		return $this->db->get_where('item', array('user_id' => $this->session->userdata("user_id")));
	}

	public function get_byid($id=''){
		if($id != NULL):
			$this->db->where('item_id', $id);
			$this->db->limit(1);
			return $this->db->get('item');
		else:
			return FALSE;
		endif;
	}

	public function do_delete_item($id=NULL){
		if($id!=NULL):
			$this->db->delete("item", array('item_id' => $id));
			return $this->db->affected_rows();
			// $this->session->set_flashdata("excluirok", "ok");
			// redirect("cliente/listar");
		endif;
	}

	public function get_all_itens()
	{
		$this->db->select("i.item_id, i.descricao, i.categoria_id, i.ingredientes, i.valor, i.status, i.image, c.categoria_id, c.nome_categoria");
		$this->db->from("item as i");
		$this->db->join("categoria as c", "c.categoria_id = i.categoria_id");
		$query = $this->db->get();
		// return $query->row();


		$arrayResult = array("item"=>array());

		foreach ($query->result_array() as $row){
			$valor = str_replace(".", ",", $row["valor"]);
			array_push($arrayResult["item"], array("id"=>$row["item_id"], "descricao"=>$row["descricao"], "nome_categoria"=>$row["nome_categoria"],
			"valor"=>$valor, "status"=>$row["status"], "image"=>$row["image"]));
		}

    	return $arrayResult;
	}

}