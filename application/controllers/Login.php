<?php if (! defined('BASEPATH')) exit('No direct script access allowed'); // linha de proteção ao controller

class Login extends CI_Controller{ // criação da classe Login

    public function index(){

        if( $this->session->userdata('usuario_logado') ) {
            redirect("home", array("tela"=>""));
        } else {
            $this->load->view('login');
        }

    }
    public function autenticar(){

        $this->load->model("Model_login", "model_login");// chama o modelo usuarios_model
        $login = $this->input->post("login");// pega via post o email que venho do formulario
        $senha = $this->input->post("senha"); // pega via post a senha que venho do formulario
        $usuario = $this->model_login->buscaPorEmailSenha($login,$senha); // acessa a função buscaPorEmailSenha do modelo

        if($usuario){
            $this->session->set_userdata("usuario_logado", $usuario);
            redirect("home", array("tela"=>""));
        }else{
            $this->session->set_flashdata("mensagem", "Usuário ou senha, inválidos!");
            $this->load->view("login");
        }
    }

    public function logout(){
        $this->session->sess_destroy();
        $dadosUsuario = array(
            'user'  => '',
            'login'     => '',
            'user_id' => '',
            'logado' => FALSE
        );
        $this->session->unset_userdata($dadosUsuario);
        redirect('login');
    }
}