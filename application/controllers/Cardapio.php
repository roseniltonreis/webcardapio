<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cardapio extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if(!$this->session->userdata("logado")){
			redirect('login');
		}

		$this->load->helper('array');
		$this->load->model("Model_cardapio", "cardapio");
	}

	public function index()
	{
		$dados = array(
			'titulo'=>'|=| Web Cardápio |=|',
			'tela'=>'',
		);
		// $this->load->view('home', $dados);
	}

	public function cadastrar(){
		$this->load->model("Model_categoria", "categoria");
		$dados = array(
			'titulo'=>'|=| Novo item de cardápio |=|',
			'tela'=>'pages/cadastrar/item',
			'categorias'=>$this->categoria->get_all()->result(),
		);
		$this->load->view("home", $dados);
	}

	public function salvar(){
		$config['upload_path'] = './assets/images/uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '0';
		// $config['max_width']  = '1024';
		// $config['max_height']  = '768';

		$this->load->library('upload', $config);

	 	if ($this->upload->do_upload('imagem')) {
            $data['img_uploaded'] = $this->upload->data();                       
        } else {
        	echo("Erro ao carregar imagem");
            // $data['error'] = $this->upload->display_errors('<div class="alert alert-error">', '</div>');                
        }
        $valoresPost = array('user_id'=> $this->input->post("user_id"),
        	'descricao'=> $this->input->post("descricao"),
        	'categoria'=> $this->input->post("categoria"),
        	'ingredientes'=> $this->input->post("ingredientes"),
        	'valor'=> floatval(str_replace(',', '.', $this->input->post("valor"))),
	        'tempo_producao'=> $this->input->post("tempo_producao"),
	        'status'=> $this->input->post("status"),
	        'image'=> $data['img_uploaded']['file_name']
		);

		$dados_form = elements(array('user_id', 'descricao','categoria','ingredientes','valor','tempo_producao','status', 'image'), $valoresPost);
		$resultSave = $this->cardapio->do_insert($dados_form);

		if($resultSave['code']){
			echo("erro|".$resultSave['code']);
		} else{
			echo "ok";
		}
	}

	public function atualizar(){

		$config['upload_path'] = './assets/images/uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '900';
		// $config['max_width']  = '1024';
		// $config['max_height']  = '768';

		$image_db = $this->cardapio->get_byid($this->input->post("item_id"))->row();
		// echo("imagem: ".$image_db->image);

		$imagemForm = $_FILES["imagem"]["name"];
		$imagemDb = $image_db->image;

		if($imagemForm == ""){
			$imagemForm = $imagemDb;

		} else{
			if(file_exists($config["upload_path"].$imagemDb))
			unlink($config["upload_path"].$imagemDb);
			$this->load->library('upload', $config);

		 	if ($this->upload->do_upload('imagem')) {
	            $data['img_uploaded'] = $this->upload->data();
	            $this->load->library('image_lib');
			    $config['image_library'] = 'gd2';
			    $config['source_image'] = $data['img_uploaded']["full_path"];
			    // $config['create_thumb'] = TRUE;
			    $config['maintain_ratio'] = TRUE;
			    $config['width']     = 150;
			    // $config['height']   = 50;

			    $this->image_lib->clear();
			    $this->image_lib->initialize($config);
			    $this->image_lib->resize();
			    // echo("redimensionou");
                    
	        } else {
	        	echo("Erro ao carregar imagem");
	            // $data['error'] = $this->upload->display_errors('<div class="alert alert-error">', '</div>');                
	        }
		}

        $valoresPost = array('descricao'=> $this->input->post("descricao"),
        	'categoria'=> $this->input->post("categoria"),
        	'ingredientes'=> $this->input->post("ingredientes"),
        	'valor'=> floatval(str_replace(',', '.', $this->input->post("valor"))),
	        'tempo_producao'=> $this->input->post("tempo_producao"),
	        'status'=> $this->input->post("status"),
	        'image'=> $imagemForm
		);

	    $dados_form = elements(array('descricao','categoria','ingredientes','valor','tempo_producao','status', 'image'), $valoresPost);
		$this->cardapio->do_update($dados_form, array("item_id"=>$this->input->post("item_id")));

		echo "ok";

	}

	public function editar(){
		$this->load->model("Model_categoria", "categoria");
		$dados = array(
			'titulo'=>'|=| Edição de Item |=|',
			'tela'=>'pages/editar/item',
			'categorias'=>$this->categoria->get_all()->result(),
		);
		$this->load->view("home", $dados);
	}

	public function excluir(){
		echo("excluir");
	}
	public function listar(){
		$dados = array(
			'titulo'=>'|=| Lista de itens do cardápio |=|',
			'tela'=>'pages/listar/item',
			'itens'=>$this->cardapio->get_all()->result(),
		);
		$this->load->view("home", $dados);
	}



}
