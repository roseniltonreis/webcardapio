<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Localizacao extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if(!$this->session->userdata("logado")){
			redirect('login');
		}
			}

	public function index()
	{
		


		$this->load->library('googlemaps');

		$config['center'] = '37.4419, -122.1419';
		$config['zoom'] = 'auto';
		$this->googlemaps->initialize($config);

		$marker = array();
		$marker['position'] = '37.429, -122.1419';
		$this->googlemaps->add_marker($marker);


		$dados = array(
			'titulo'=>'|=| Web Cardápio |=|',
			'tela'=>'localizacao',
			'map'=>$this->googlemaps->create_map()
		);


		$this->load->view('home', $dados);
	}


}
