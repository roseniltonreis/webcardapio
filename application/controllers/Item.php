<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if(!$this->session->userdata("logado")){
			redirect('login');
		}
		$this->load->helper('array');
		$this->load->model("Model_item", "item");
	}

	public function index()
	{
		$dados = array(
			'titulo'=>'|=| Web Cardápio |=|',
			'tela'=>'',
		);
		// $this->load->view('home', $dados);
	}

	public function cadastrar(){
		$this->load->model("Model_categoria", "categoria");
		$dados = array(
			'titulo'=>'|=| Novo item de cardápio |=|',
			'tela'=>'pages/cadastrar/item',
			'categorias'=>$this->categoria->get_all()->result(),
		);
		$this->load->view("home", $dados);
	}

	public function salvar(){

		$config['upload_path'] = './assets/images/uploads/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '0';
		$config['encrypt_name'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		// $config['max_width']  = '1024';
		// $config['max_height']  = '768';


		$imagemForm = $_FILES["imagem"]["name"]; //imagem que veio do form
		$imageName = "";
		$this->load->library('upload', $config);


        if($imagemForm != ""){

		 	if ($this->upload->do_upload('imagem')) {
	            $data['imagem'] = $this->upload->data();
	            $this->load->library('image_lib');
	            $config['image_library'] = 'gd2';
	            $config['source_image'] = $data['imagem']["full_path"];
	            $config['width']     = 200;

	            $this->image_lib->clear();
	            $this->image_lib->initialize($config);
	            $this->image_lib->resize();

	        } else {
	        	// echo("Erro ao carregar imagem");
	            $data['error'] = $this->upload->display_errors('<div class="alert alert-error">', '</div>');
	        }

	        $imageName = $data["imagem"]["file_name"];
        	
        } else{
        	$imageName = "";
        }

        $valoresPost = array('user_id'=> $this->input->post("user_id"),
        	'descricao'=> $this->input->post("descricao"),
        	'categoria_id'=> $this->input->post("categoria_id"),
        	'ingredientes'=> $this->input->post("ingredientes"),
        	'valor'=> floatval(str_replace(',', '.', $this->input->post("valor"))),,
	        // 'tempo_producao'=> $this->input->post("tempo_producao"),
	        'status'=> $this->input->post("status"),
	        'image'=> $imageName
		);

		$dados_form = elements(array('user_id', 'descricao','categoria_id','ingredientes','valor',/*'tempo_producao',*/'status', 'image'), $valoresPost);
		$resultSave = $this->item->do_insert($dados_form);

		if($resultSave['code']){
			echo("erro|".$resultSave['code']);
		} else{
			echo "ok";
		}
	}

	public function atualizar(){

		$config['upload_path'] = './assets/images/uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '0';
		$config['encrypt_name'] = TRUE;
		// $config['max_width']  = '1024';
		// $config['max_height']  = '768';

		$image_db = $this->item->get_byid($this->input->post("item_id"))->row();
		// echo("imagem: ".$image_db->image);

		$imagemForm = $_FILES["imagem"]["name"]; //imagem que veio do form
		$imagemDb = $image_db->image; //imagem do banco

		//Salva dados no banco			    
	       
		$this->load->library('upload', $config);

		if($imagemForm !=""){
			//Deleta imagem antiga
           	if(file_exists($config["upload_path"].$imagemDb))
			unlink($config["upload_path"].$imagemDb);

				if ($this->upload->do_upload('imagem')) {
		            $data['img_uploaded'] = $this->upload->data();
		            $this->load->library('image_lib');
				    $config['image_library'] = 'gd2';
				    $config['source_image'] = $data['img_uploaded']["full_path"];
				    // $config['create_thumb'] = TRUE;
				    $config['remove_spaces'] = TRUE;
				    $config['maintain_ratio'] = TRUE;
				    $config['width']     = 200;
				    // $config['height']   = 50;

				    $this->image_lib->clear();
				    $this->image_lib->initialize($config);
				    $this->image_lib->resize();

				    $nomeImagem = $data['img_uploaded']['file_name'];
		 		} else {
		        	echo("erro");
		            // $data['error'] = $this->upload->display_errors('<div class="alert alert-error">', '</div>');
		        }

			}else{
				$nomeImagem = $imagemDb;
			}
		    //Salva dados no banco			    
	        $valoresPost = array('descricao'=> $this->input->post("descricao"),
	        	'categoria_id'=> $this->input->post("categoria_id"),
	        	'ingredientes'=> $this->input->post("ingredientes"),
	        	'valor'=> floatval(str_replace(',', '.', $this->input->post("valor"))),,
		        'tempo_producao'=> $this->input->post("tempo_producao"),
		        'status'=> $this->input->post("status"),
		        'image'=> $nomeImagem
			);
		    $dados_form = elements(array('descricao','categoria_id','ingredientes','valor','tempo_producao','status', 'image'), $valoresPost);
			$this->item->do_update($dados_form, array("item_id"=>$this->input->post("item_id")));
           	
           	echo "ok";

       
	}

	public function editar(){
		$this->load->model("Model_categoria", "categoria");
		$dados = array(
			'titulo'=>'|=| Edição de Item |=|',
			'tela'=>'pages/editar/item',
			'categorias'=>$this->categoria->get_all()->result(),
		);
		$this->load->view("home", $dados);
	}

	public function excluir(){
		$id = $this->input->post("id");
		$nomeImagem = $this->item->get_byid($id)->row()->image;

		//deleta imagem do server
		$image_path = './assets/images/uploads/';
		if(file_exists($image_path.$nomeImagem) && $nomeImagem != ""){
			unlink($image_path.$nomeImagem);
		}

		$result =  $this->item->do_delete_item($id);

		if($result > 0){
			echo "ok";
		} else{
			echo "erro";
		}

	}
	public function listar(){
		$dados = array(
			'titulo'=>'|=| Lista de itens do cardápio |=|',
			'tela'=>'pages/listar/item',
			'itens'=>$this->item->get_all()->result(),
		);
		$this->load->view("home", $dados);
	}

	public function get_itens(){
		$result = $this->item->get_all_itens();

    	echo json_encode($result);
	}



}
