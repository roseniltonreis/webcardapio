<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		if(!$this->session->userdata("logado")){
			redirect('login');
		}

		$this->load->helper('array');
		$this->load->model("Model_categoria", "categoria");
	}

	public function index()
	{
		$dados = array(
			'titulo'=>'|=| Web Cardápio |=|',
			'tela'=>'',
		);
		// $this->load->view('home', $dados);
	}

	public function cadastrar(){
		$dados = array(
			'titulo'=>'|=| Nova categoria |=|',
			'tela'=>'pages/cadastrar/categoria',
		);
		$this->load->view("home", $dados);
	}

	public function salvar(){
		$categoria_exists = $this->categoria->category_exists($this->input->post('nome_categoria'));

		if($categoria_exists->num_rows() > 0){
			echo("Erro, categoria já cadastrada!");
		}else{
			$this->categoria->do_insert($this->input->post());
			echo "ok";
		}
	}

	public function editar(){
		$dados = array(
			'titulo'=>'Modo Edição',
			'tela'=>'pages/editar/categoria',
		);
		$this->load->view("home", $dados);
	}

	public function updateCategoria(){
		$dados_form = elements(array('nome_categoria'), $this->input->post());
		$retornoDb = $this->categoria->do_update($dados_form, array('categoria_id'=>$this->input->post('idcategoria')));
		if(!$retornoDb["code"]){
			echo("ok");
		}else{
			echo("Erro: ".$retornoDb["code"]." -> ".$retornoDb["message"]);
		}
	}

	public function excluir(){

		$iduser = $this->input->post("id");
		$result = $this->categoria->do_delete(array('categoria_id'=>$iduser));

		if($result > 0){
			echo("ok");
		} else{
			echo("erro");
		}
	}
	public function listar(){
		$dados = array(
			'titulo'=>'|=| Lista de itens do cardápio |=|',
			'tela'=>'pages/listar/categoria',
			'categorias'=>$this->categoria->get_all()->result(),
		);
		$this->load->view("home", $dados);
	}
}
