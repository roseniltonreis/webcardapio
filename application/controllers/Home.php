<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if(!$this->session->userdata("logado")){
			redirect('login');
		}
	}

	public function index()
	{
		$dados = array(
			'titulo'=>'|=| Web Cardápio |=|',
			'tela'=>'',
		);
		$this->load->view('home', $dados);
	}

	public function config(){
		$dados = array(
			'titulo'=>'|=| Configurações |=|',
			'tela'=>'pages/config',
		);
		$this->load->view('home', $dados);
	}

	public function localizacao(){
		$dados = array(
			'titulo'=>'|=| Localização |=|',
			'tela'=>'localizacao',
		);
		$this->load->view('home', $dados);
	}



}
