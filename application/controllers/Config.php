<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if(!$this->session->userdata("logado")){
			redirect('login');
		}

		$this->load->helper('array');
		$this->load->model("Model_config", "model_config");
	}

	public function index()
	{
		$dados = array(
			'titulo'=>'|=| Configurações |=|',
			'tela'=>'pages/config',
		);
		$this->load->view('home', $dados);
	}

	public function salvar(){

		$checks = $this->input->post("dia");
		$cep = $this->input->post("cep");
		$rua = $this->input->post("rua");
		$cidade = $this->input->post("cidade");
		$numero = $this->input->post("numero");
		$bairro = $this->input->post("bairro");
		$nome_estabelecimento = $this->input->post("nomeestab");
		$user_id = $this->input->post("user_id");
		$dias_funcionamento = $this->input->post("dias_funcionamento");

		$arrayDiasFuncionamento = array();

		$arrayAll = array("user_id" => $user_id, "nome_estabelecimento" => $nome_estabelecimento, "cep" => str_replace("-", "", $cep), "rua" => $rua,
			"numero" => $numero, "bairro" => $bairro, "cidade" => $cidade, "dias_funcionamento" => $dias_funcionamento);

		$dados_form = elements(array('user_id', 'nome_estabelecimento','cep','rua','numero','bairro','cidade', 'dias_funcionamento'), $arrayAll);


		$resultado = $this->model_config->get_by_user_id($user_id);

		// echo($resultado->num_rows());

		if($resultado->num_rows() > 0){
			// echo("dando updadte...");
			$this->model_config->do_update($dados_form, array('user_id'=>$user_id));
			echo("ok");
		}else{
			// echo("cria novo registro!");
			$this->model_config->do_insert($dados_form);
			echo("ok");
		}


		// if(!$retornoDb["code"]){
		// 	echo("ok");
		// }else{
		// 	echo("Erro: ".$retornoDb["code"]." -> ".$retornoDb["message"]);
		// }
	}
}
